package com.bank.transactions;

import com.bank.models.*;
import com.bank.repositories.TransactionRepository;
import org.junit.ClassRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Testcontainers
@ActiveProfiles("test-containers")
@SpringBootTest
@ContextConfiguration(initializers = {TransactionRepositoryTCIntegrationTest.Initializer.class})
public class TransactionRepositoryTCIntegrationTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:14.2")
            .withDatabaseName("postgres")
            .withUsername("postgres")
            .withPassword("postgres");

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgreSQLContainer.start();
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword(),
                    "spring.jpa.hibernate.ddl-auto=create-drop"
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Test
    public void testSaveAndReadTransactions() {

        User Billy = new User(1L, "billy@mail.ru", "idol", "Billy", "Idol", LocalDateTime.now(), Role.USER, false);
        Account userId1Account1 = new Account(1L, Billy.getId(), 0.0, AccountState.ACTIVE, LocalDateTime.now());
        Account userId1Account2 = new Account(2L, Billy.getId(), 10000.0, AccountState.ACTIVE, LocalDateTime.now());

        Transaction transaction = new Transaction(1L, Billy.getId(), Instant.now(), userId1Account2.getId(), userId1Account1.getId(), 3000.0, null, null);

        List<Transaction> transactionList = Collections.singletonList(transaction);

        transactionRepository.saveAll(transactionList);

        List<Transaction> transactions = transactionRepository.findAll();

        assertThat(transactions).isNotNull();
        Assertions.assertSame(transactionList.size(), 1);
        assertThat(transactions.get(0).getId()).isEqualTo(1L);
        assertThat(transactions.get(0).getUserId()).isEqualTo(1L);
        assertThat(transactions.get(0).getAmountTransferred()).isEqualTo(3000L);
    }
}
