package com.bank.cashback;

import com.bank.exceptions.MCCCodeNotFoundException;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Role;
import com.bank.models.User;
import com.bank.repositories.AccountRepository;
import com.bank.repositories.TransactionRepository;
import com.bank.repositories.UserRepository;
import com.bank.services.impl.AccountServiceImpl;
import com.bank.services.impl.TransactionServiceImpl;
import com.bank.services.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CashbackService {

    @Mock
    private UserRepository userRepository;
    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @InjectMocks
    private AccountServiceImpl accountService;

    @InjectMocks
    private UserServiceImpl userService;

    User user1 = new User(1L, "user1@yahoo.com", "user", "Igor", "Sirotin", LocalDateTime.now(), Role.USER, false);
    User user2 = new User(20L, "user2@yahoo.com", "user", "Artem", "Pirozhkov", LocalDateTime.now(), Role.USER, false);

    Account user1Account1 = new Account(1L, user1.getId(), 20000.0, AccountState.ACTIVE, LocalDateTime.now());
    Account user1Account2 = new Account(2L, user1.getId(), 80000.0, AccountState.ACTIVE, LocalDateTime.now());

    Account user2Account1 = new Account(1L, user2.getId(), 20000.0, AccountState.ACTIVE, LocalDateTime.now());
    Account user2Account2 = new Account(2L, user2.getId(), 80000.0, AccountState.ACTIVE, LocalDateTime.now());

    @Test
    public void testDefaultCashback() {
        when(accountRepository.findById(user1Account1.getId())).thenReturn(Optional.of(user1Account1));
        when(accountRepository.findById(user2Account2.getId())).thenReturn(Optional.of(user2Account2));
        transactionService.makeTransaction(user2.getId(), user2Account2.getId(), user1Account1.getId(), 10000.0, 5555);
        assertEquals("", user2Account2.getBalance(), 70100.0);
        assertEquals("", user1Account1.getBalance(), 30000.0);
    }

    @Test
    public void testIncreasedCashback() {
        when(accountRepository.findById(user1Account1.getId())).thenReturn(Optional.of(user1Account1));
        when(accountRepository.findById(user2Account2.getId())).thenReturn(Optional.of(user2Account2));
        transactionService.makeTransaction(user2.getId(), user2Account2.getId(), user1Account1.getId(), 10000.0, 5949);
        assertEquals("", user2Account2.getBalance(), 70300.0);
        assertEquals("", user1Account1.getBalance(), 30000.0);
    }

    @Test
    public void testCashbackBetweenYourAccountsWithMCCCode() {
        when(accountRepository.findById(user1Account1.getId())).thenReturn(Optional.of(user1Account1));
        when(accountRepository.findById(user1Account2.getId())).thenReturn(Optional.of(user1Account2));
        transactionService.makeTransaction(user1.getId(), user1Account2.getId(), user1Account1.getId(), 10000.0, 5555);
        assertEquals("", user1Account1.getBalance(), 30000.0);
        assertEquals("", user1Account2.getBalance(), 70000.0);
    }

    @Test
    public void testCashbackBetweenYourAccountsWithoutMCCCode() {
        when(accountRepository.findById(user1Account1.getId())).thenReturn(Optional.of(user1Account1));
        when(accountRepository.findById(user1Account2.getId())).thenReturn(Optional.of(user1Account2));
        transactionService.makeTransaction(user1.getId(), user1Account2.getId(), user1Account1.getId(), 10000.0, null);
        assertEquals("", user1Account1.getBalance(), 30000.0);
        assertEquals("", user1Account2.getBalance(), 70000.0);
    }

    @Test(expected = MCCCodeNotFoundException.class)
    public void testMCCCodeNotFoundPositiveNumber() {
        when(accountRepository.findById(user2Account1.getId())).thenReturn(Optional.of(user2Account1));
        when(accountRepository.findById(user1Account2.getId())).thenReturn(Optional.of(user1Account2));
        transactionService.makeTransaction(user1.getId(), user1Account2.getId(), user2Account1.getId(), 10000.0, 100000);
    }

    @Test(expected = MCCCodeNotFoundException.class)
    public void testMCCCodeNotFoundNegativeNumber() {
        when(accountRepository.findById(user2Account1.getId())).thenReturn(Optional.of(user2Account1));
        when(accountRepository.findById(user1Account2.getId())).thenReturn(Optional.of(user1Account2));
        transactionService.makeTransaction(user1.getId(), user1Account2.getId(), user2Account1.getId(), 10000.0, -99999);
    }

    @Test(expected = NumberFormatException.class)
    public void testMCCCodeNumberFormatExceptionNegative() {
        transactionService.makeTransaction(user1.getId(), user1Account2.getId(), user2Account1.getId(), 10000.0, Integer.valueOf(("dwdd33fd")));
    }
}
