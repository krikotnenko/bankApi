package com.bank.accounts;

import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Role;
import com.bank.models.User;
import com.bank.repositories.AccountRepository;
import org.junit.ClassRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Testcontainers
@ActiveProfiles("test-containers")
@SpringBootTest
@ContextConfiguration(initializers = {AccountRepositoryTCIntegrationTest.Initializer.class})
public class AccountRepositoryTCIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:14.2")
            .withDatabaseName("postgres")
            .withUsername("postgres")
            .withPassword("postgres");

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgreSQLContainer.start();
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword(),
                    "spring.jpa.hibernate.ddl-auto=create-drop"
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @Test
    public void testSaveAndReadTransactions() {

        User Billy = new User(1L, "billy@mail.ru", "idol", "Billy", "Idol", LocalDateTime.now(), Role.USER, false);
        Account userId1Account1 = new Account(1L, Billy.getId(), 0.0, AccountState.ACTIVE, LocalDateTime.now());
        Account userId1Account2 = new Account(2L, Billy.getId(), 10000.0, AccountState.ACTIVE, LocalDateTime.now());

        List<Account> accountList = Arrays.asList(userId1Account1, userId1Account2);

        accountRepository.saveAll(accountList);

        List<Account> accounts = accountRepository.findAll();

        assertThat(accounts).isNotNull();
        Assertions.assertSame(accountList.size(), 2);
        assertThat(accounts.get(0).getId()).isEqualTo(1L);
        assertThat(accounts.get(1).getId()).isEqualTo(2L);
        assertThat(accounts.get(0).getBalance()).isEqualTo(0.0);
        assertThat(accounts.get(1).getBalance()).isEqualTo(10000.0);
    }
}
