package com.bank.accounts;

import com.bank.BankApiApplication;
import com.bank.config.ContainersEnvironment;
import com.bank.models.Account;
import com.bank.repositories.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
@ActiveProfiles("test-containers")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BankApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountRepositoryIntegrationTest extends ContainersEnvironment {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testGetAccountsExpectEmptyList() {
        List<Account> list = accountRepository.findAll();
        assertEquals(0, list.size());
    }
}
