create table users
(
    id         bigserial primary key,
    date       timestamp    not null,
    email      varchar(255) not null,
    first_name varchar(255) not null,
    is_blocked boolean      not null,
    last_name  varchar(255) not null,
    password   varchar(255) not null,
    role       varchar(255) not null
);

create table accounts
(
    id      bigserial primary key,
    balance double precision not null,
    date    timestamp        not null,
    state   text             not null,
    user_id bigserial        not null
        references users (id)
);

create table transactions
(
    id                 bigserial primary key,
    amount_transferred double precision not null,
    operation_time     timestamp        not null,
    source_account_id  bigint           not null,
    target_account_id  bigint           not null,
    mcc                int,
    transaction_type   text,
    user_id            bigserial        not null
        references users (id)
);

