package com.bank.models;

public enum TransactionType {
    PAYMENT, TRANSFER
}
