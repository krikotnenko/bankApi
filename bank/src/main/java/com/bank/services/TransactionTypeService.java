package com.bank.services;

import com.bank.models.Transaction;
import com.bank.models.TransactionType;

public class TransactionTypeService {

    public static void choiceTransactionType(Transaction transaction, Integer mcc) {
        if (mcc != null && mcc >= 1000 && mcc <= 9999) {
            transaction.setTransactionType(TransactionType.PAYMENT);
        } else {
            transaction.setTransactionType(TransactionType.TRANSFER);
        }
    }
}
