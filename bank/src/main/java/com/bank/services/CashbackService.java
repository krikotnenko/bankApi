package com.bank.services;


import com.bank.constants.CashbackPercentValueConstants;
import com.bank.constants.MCCConstants;
import com.bank.exceptions.MCCCodeNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class CashbackService {
    private static final List<Integer> favouriteCashback = MCCConstants.CLOTHES_SHOES;

    public static double checkIntervalCashback(Long userId, Double amountTransferred, Integer mcc) {

        double result = 0;

        if (mcc != null) {
            if (favouriteCashback.contains(mcc)) {
                result = getIncreasedCashbackValue(amountTransferred);
                log.info(("User with ID {}" + " received an increased cashback of " + getIncreasedCashbackValue(amountTransferred) + " RUB"), userId);
            } else if (mcc >= 1000 && mcc <= 9999) {
                result = getDefaultCashbackValue(amountTransferred);
                log.info(("User with ID {}" + " received a cashback of " + getDefaultCashbackValue(amountTransferred) + " RUB"), userId);
            } else {
                log.error("Transfer failed due to MCC code {} does not exist. If this error occurs again, please contact the developers", mcc);
                throw new MCCCodeNotFoundException(mcc);
            }
        }
        return result;
    }

    private static double getIncreasedCashbackValue(Double amountTransferred) {

        double increasedCashbackPercentValue = CashbackPercentValueConstants.increasedCashbackPercentValue;
        double cashbackValue = 0;

        if (amountTransferred % 10 == 0) {
            cashbackValue = amountTransferred * increasedCashbackPercentValue;
        } else {
            cashbackValue += Math.round(Math.round(amountTransferred) * increasedCashbackPercentValue);
        }
        return cashbackValue;
    }

    private static double getDefaultCashbackValue(Double amountTransferred) {

        double defaultCashbackValuePercentValue = CashbackPercentValueConstants.defaultCashbackPercentValue;
        double cashbackValue = 0;

        if (amountTransferred % 10 == 0) {
            cashbackValue = amountTransferred * defaultCashbackValuePercentValue;
        } else {
            cashbackValue = Math.round(Math.round(amountTransferred) * defaultCashbackValuePercentValue);
        }
        return cashbackValue;
    }

}