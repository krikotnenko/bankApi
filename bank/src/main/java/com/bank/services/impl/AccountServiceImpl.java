package com.bank.services.impl;

import com.bank.exceptions.*;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.User;
import com.bank.repositories.AccountRepository;
import com.bank.repositories.UserRepository;
import com.bank.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.bank.models.AccountState.ACTIVE;
import static com.bank.models.AccountState.CLOSED;

@Service
@Slf4j
@Transactional
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final UserServiceImpl userService;
    private final UserRepository userRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, UserServiceImpl userService, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    public Account createAccount(Long userId, Double initialBalance) {
        Optional<User> existingUser = Optional.ofNullable(userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId)));
        if (existingUser.isPresent() && initialBalance >= 0 && !existingUser.get().isBlocked()) {
            Account account = new Account();
            account.setUserId(userId);
            account.setBalance(initialBalance);
            account.setState(ACTIVE);
            account.setDateCreated(LocalDateTime.now());
            accountRepository.save(account);
            log.info("Account with ID {} is created", account.getId());
            return account;
        } else if (initialBalance < 0 && existingUser.get().isBlocked()) {
            log.error("User ID {} is BLOCKED and initial balance could not be less than zero. Please contact the bank's technical support service to unblock your account", userId);
            throw new UserIsBlockedAndInitialBalanceLessThanZeroException("Initial balance could not be less than zero");
        } else if (existingUser.get().isBlocked()) {
            log.error("User ID {} is BLOCKED. Please contact the bank's technical support service to unblock your account", userId);
            throw new UserIsBlockedException("User ID is BLOCKED. Please contact the bank's technical support service to unblock your account");
        } else {
            log.error("Initial balance could not be less than zero");
            throw new InitialBalanceLessThanZeroException("Initial balance could not be less than zero");
        }
    }

    @Override
    public List<Account> getAccounts(Long userId) {
        User existingUser = this.userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
        List<Account> accounts = new ArrayList<>();
        for (Account account : accountRepository.findAll()) {
            if (account.getUserId().equals(existingUser.getId())) {
                accounts.add(account);
            }
        }
        return accounts;
    }

    public Account getAccount(Long accountId) {
        return this.accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    @Override
    public void blockAccount(Long userId, Long accountId) {
        try {
            if (userId == null) {
                throw new AccountNotFoundException(accountId);
            } else {
                Account account = getAccount(accountId);
                switch (account.getState()) {
                    case CLOSED:
                        log.error("Account with ID {} is already CLOSED", accountId);
                        throw new IllegalStateException("Account is already CLOSED");
                    case BLOCKED:
                        log.error("Account with ID {} is already BLOCKED", accountId);
                        throw new IllegalStateException("Account is already BLOCKED");
                    case ACTIVE:
                        account.setState(AccountState.BLOCKED);
                        this.accountRepository.save(account);
                        log.info("Account with ID {} is BLOCKED", accountId);
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e);
        }
    }

    @Override
    public void unblockAccount(Long userId, Long accountId) {
        try {
            if (userId == null) {
                throw new AccountNotFoundException(accountId);
            } else {
                Account account = getAccount(accountId);
                switch (account.getState()) {
                    case CLOSED:
                        log.error("Account with ID {} is already CLOSED", accountId);
                        throw new IllegalStateException("Account is already CLOSED");
                    case ACTIVE:
                        log.error("Account with ID {} is already ACTIVE", accountId);
                        throw new IllegalStateException("Account is already ACTIVE");
                    case BLOCKED:
                        account.setState(ACTIVE);
                        this.accountRepository.save(account);
                        log.info("Account with ID {} is ACTIVE", accountId);
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e);
        }
    }

    @Override
    public void closeAccount(Long userId, Long accountId) {
        try {
            if (userId == null) {
                log.error("Account ID {} is not exist", accountId);
                throw new AccountNotFoundException(accountId);
            } else {
                Account account = getAccount(accountId);
                switch (account.getState()) {
                    case CLOSED:
                        log.error("Account ID {} is already CLOSED", accountId);
                        throw new IllegalStateException("Account is already CLOSED");
                    case BLOCKED:
                        account.setState(CLOSED);
                        this.accountRepository.save(account);
                        log.info("Account ID {} is CLOSED", accountId);
                    case ACTIVE:
                        account.setState(CLOSED);
                        this.accountRepository.save(account);
                        log.info("Account ID {} is CLOSED", accountId);
                }
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error " + e);
        }
    }

    @Override
    public Account updateAccount(Long accountId, Account account) {
        Account existingAccount = this.accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
        existingAccount.setUserId(account.getUserId());
        existingAccount.setBalance(account.getBalance());
        existingAccount.setState(account.getState());
        existingAccount.setDateCreated(account.getDateCreated());
        this.accountRepository.save(existingAccount);
        log.info("Account ID {} is updated", accountId);
        return existingAccount;
    }

    @Override
    public void deleteAccount(Long accountId) {
        Account existingAccount = this.accountRepository.findById(accountId)
                .orElseThrow(() -> new AccountNotFoundException(accountId));
        this.accountRepository.delete(existingAccount);
        log.info("Account with ID {} is deleted", accountId);
    }
}