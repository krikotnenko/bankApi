package com.bank.services.impl;

import com.bank.exceptions.UserAlreadyExistsException;
import com.bank.exceptions.UserNotFoundException;
import com.bank.models.User;
import com.bank.repositories.UserRepository;
import com.bank.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(User user) {
        Optional<User> userByEmail = userRepository.findByEmail(user.getEmail());
        if (userByEmail.isPresent()) {
            log.error("User is already exists. Please use different email from {}", user.getEmail());
            throw new UserAlreadyExistsException("User is already exists. Please use different email.");
        }
        user.setDateCreated(LocalDateTime.now());
        userRepository.save(user);
        log.info("User with ID {} is created", user.getId());
        return user;
    }

    @Override
    public User getUser(Long userId) {
        return this.userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
    }

    @Override
    public List<User> getUsers(){
        return userRepository.findAll().stream().limit(10).collect(Collectors.toList());
    }

    @Override
    public void blockUser(Long userId) {
        User existingUser = getUser(userId);
        if (userId == null) {
            throw new UserNotFoundException(userId);
        } else {
            existingUser.setBlocked(true);
            this.userRepository.save(existingUser);
        }
    }

    @Override
    public void unblockUser(Long userId) {
        User existingUser = getUser(userId);
        if (userId == null) {
            log.error("User with ID {} is not exist", userId);
            throw new UserNotFoundException(userId);
        } else {
            if (existingUser.isBlocked()) {
                existingUser.setBlocked(false);
                this.userRepository.save(existingUser);
            }
        }
    }

    @Override
    public User updateUser(User user, Long userId) {
        User existingUser = this.userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
        existingUser.setEmail(user.getEmail());
        existingUser.setPassword(user.getPassword());
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());
        existingUser.setDateCreated(LocalDateTime.now());
        existingUser.setRole(user.getRole());
        existingUser.setBlocked(user.isBlocked());
        this.userRepository.save(existingUser);
        log.info("User with ID {} is updated", userId);
        return existingUser;
    }

    public void deleteUser(Long userId) {
        User existingUser = this.userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
        this.userRepository.delete(existingUser);
        log.info("User with ID {} is deleted", userId);
    }
}