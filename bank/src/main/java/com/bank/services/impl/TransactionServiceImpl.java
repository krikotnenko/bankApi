package com.bank.services.impl;

import com.bank.exceptions.*;
import com.bank.models.Account;
import com.bank.models.AccountState;
import com.bank.models.Transaction;
import com.bank.models.User;
import com.bank.repositories.AccountRepository;
import com.bank.repositories.TransactionRepository;
import com.bank.repositories.UserRepository;
import com.bank.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.bank.services.CashbackService.checkIntervalCashback;
import static com.bank.services.TransactionTypeService.choiceTransactionType;

@Service
@Slf4j
@Transactional
public class TransactionServiceImpl implements TransactionService {
    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    private static int count = 0;


    @Autowired
    public TransactionServiceImpl(UserRepository userRepository, TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    public void makeTransaction(Long userId, Long sourceAccountId, Long targetAccountId, Double amountTransferred, Integer mcc) {
        Optional<Account> sourceAccount = Optional.ofNullable(accountRepository.findById(sourceAccountId).orElseThrow(() -> new AccountNotFoundException(sourceAccountId)));
        Optional<Account> targetAccount = Optional.ofNullable(accountRepository.findById(targetAccountId).orElseThrow(() -> new AccountNotFoundException(targetAccountId)));

        if (sourceAccount.isPresent() && targetAccount.isPresent() && !sourceAccountId.equals(targetAccountId)) {
            log.info("Request for a money transfer has been received from user ID {}", userId);
            if (sourceAccount.get().getBalance() >= amountTransferred && amountTransferred > 0.001 && sourceAccount.get().getState().equals(AccountState.ACTIVE) && targetAccount.get().getState().equals(AccountState.ACTIVE)) {
                Transaction transaction = new Transaction();
                transaction.setUserId(userId);
                transaction.setOperationTime(Instant.now());
                transaction.setSourceAccountId(sourceAccountId);
                transaction.setTargetAccountId(targetAccountId);
                transaction.setAmountTransferred(amountTransferred);
                transaction.setMcc(mcc);
                choiceTransactionType(transaction, mcc);
                transactionRepository.save(transaction);
                log.info("Transaction with ID {} is created", transaction.getId());

                log.info("Initial balance in the sender's account with ID {} ", sourceAccount.get().getId() + " before transaction is " + sourceAccount.get().getBalance() + " RUB");
                log.info("Initial balance in the recipient's account with ID {} ", targetAccount.get().getId() + " before transaction is " + targetAccount.get().getBalance() + " RUB");

                sourceAccount.get().setBalance(sourceAccount.get().getBalance() - amountTransferred);
                log.info("Withdrawal from the sender's account with ID {} ", sourceAccount.get().getId() + " in the amount of " + amountTransferred + " RUB");

                if (!Objects.equals(sourceAccount.get().getUserId(), targetAccount.get().getUserId())) {
                    sourceAccount.get().setBalance(sourceAccount.get().getBalance() + checkIntervalCashback(userId, amountTransferred, mcc));
                }

                log.info("The amount of money in the sender's account after transaction is " + sourceAccount.get().getBalance() + " RUB");

                targetAccount.get().setBalance(targetAccount.get().getBalance() + amountTransferred);
                log.info("The amount of money in the recipient's account after transaction is " + targetAccount.get().getBalance() + " RUB");

            } else if (!sourceAccount.get().getState().equals(AccountState.ACTIVE) || (!targetAccount.get().getState().equals(AccountState.ACTIVE))) {
                log.error("An exception occurred while trying to transfer money from account {} to account {}", sourceAccountId, targetAccountId);
                if (sourceAccount.get().getState().equals(AccountState.BLOCKED)) {
                    log.error("Sender's account {} is BLOCKED", sourceAccountId);
                    throw new IllegalStateException("Sender's account is BLOCKED");
                } else if (targetAccount.get().getState().equals(AccountState.BLOCKED)) {
                    log.error("Recipient's account {} is BLOCKED", targetAccountId);
                    throw new IllegalStateException("Recipient's account is BLOCKED");
                }
                if (sourceAccount.get().getState().equals(AccountState.CLOSED)) {
                    log.error("Sender's account {} is CLOSED", sourceAccountId);
                    throw new IllegalStateException("Sender's account is CLOSED");
                } else if (targetAccount.get().getState().equals(AccountState.CLOSED)) {
                    log.error("Recipient's account {} is CLOSED", targetAccountId);
                    throw new IllegalStateException("Recipient's account is CLOSED");
                }
            } else if (amountTransferred < 0) {
                log.error("Amount of money to transfer cannot be less than zero. You tried to transfer {} RUB", amountTransferred);
                throw new AmountTransferredLessThanZeroException("Amount of money to transfer cannot be less than zero");
            } else if (sourceAccount.get().getBalance() < amountTransferred) {
                log.error("Not enough money to make a transfer to recipient's account with ID {}. You are trying to transfer a negative amount of {} RUB", targetAccountId, amountTransferred);
                throw new InsufficientAmountMoneyException("Not enough money to make a transfer to recipient's account");
            }
        } else if (sourceAccountId.equals(targetAccountId)) {
            log.error("Sender's account ID could not be equal with recipient's account ID");
            throw new AccountIDMatchException("The account IDs of the sender and recipient cannot be the same");
        }
    }

    @Override
    public List<Transaction> getTransactionsByUser(Long userId) {
        User existingUser = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
        List<Transaction> transactionsByUser = new ArrayList<>();
        for (Transaction transaction : transactionRepository.findAll()) {
            if (transaction.getUserId().equals(existingUser.getId())) {
                transactionRepository.save(transaction);
                transactionsByUser.add(transaction);
            }
        }
        return transactionsByUser;
    }

    @Override
    public List<Transaction> getTransactionsByAccount(Long accountId) {
        Account existingAccount = accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
        List<Transaction> transactionsByAccount = new ArrayList<>();
        for (Transaction transaction : transactionRepository.findAll()) {
            if (transaction.getSourceAccountId().equals(existingAccount.getId()) || transaction.getTargetAccountId().equals(existingAccount.getId())) {
                transactionRepository.save(transaction);
                transactionsByAccount.add(transaction);
            }
        }
        return transactionsByAccount;
    }

    @Override
    public List<Transaction> getTransactionsByUserAndAccount(Long userId, Long accountId) {
        User existingUser = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
        Account existingAccount = accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
        List<Transaction> transactionsByUserAndAccount = new ArrayList<>();
        for (Transaction transaction : transactionRepository.findAll()) {
            if (transaction.getUserId().equals(existingUser.getId()) && ((transaction.getSourceAccountId().equals(existingAccount.getId()) && transaction.getTargetAccountId().equals(existingAccount.getId()))
                    || transaction.getSourceAccountId().equals(existingAccount.getId()) || transaction.getTargetAccountId().equals(existingAccount.getId()))) {
                transactionRepository.save(transaction);
                transactionsByUserAndAccount.add(transaction);
            }
        }
        return transactionsByUserAndAccount;
    }

    @Override
    public Transaction getTransaction(Long transactionId) {
        return this.transactionRepository.findById(transactionId).orElseThrow(() -> new TransactionNotFoundException(transactionId));
    }

    @Override
    public Transaction updateTransaction(Transaction transaction) {
        Transaction existingTransaction = this.transactionRepository.findById(transaction.getId()).orElseThrow(() -> new TransactionNotFoundException(transaction.getId()));
        existingTransaction.setId(transaction.getId());
        existingTransaction.setUserId(transaction.getUserId());
        existingTransaction.setOperationTime(transaction.getOperationTime());
        existingTransaction.setAmountTransferred(transaction.getAmountTransferred());
        transactionRepository.save(existingTransaction);
        log.info("Transaction with ID {} is updated", transaction.getId());
        return existingTransaction;
    }

    @Override
    public void deleteTransaction(Long transactionId) {
        Transaction existingTransaction = this.transactionRepository.findById(transactionId)
                .orElseThrow(() -> new TransactionNotFoundException(transactionId));
        this.transactionRepository.delete(existingTransaction);
        log.info("Transaction with ID {} is deleted", transactionId);
    }
}

