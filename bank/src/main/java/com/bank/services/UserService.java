package com.bank.services;

import com.bank.models.User;

import java.util.List;

public interface UserService {
    User createUser(User user);

    List<User> getUsers();

    User getUser(Long userId);

    void blockUser(Long userId);

    void unblockUser(Long userId);

    User updateUser(User user, Long userId);

    void deleteUser(Long userId);
}