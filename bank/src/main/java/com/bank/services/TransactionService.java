package com.bank.services;

import com.bank.models.Transaction;

import java.util.List;

public interface TransactionService {
    void makeTransaction(Long userId, Long sourceAccountId, Long targetAccountId, Double amountTransferred, Integer mcc);

    List<Transaction> getTransactionsByUser(Long userId);

    List<Transaction> getTransactionsByAccount(Long accountId);

    List<Transaction> getTransactionsByUserAndAccount(Long userId, Long accountId);

    Transaction getTransaction(Long transactionId);

    Transaction updateTransaction(Transaction transaction);

    void deleteTransaction(Long transactionId);
}