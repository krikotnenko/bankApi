package com.bank.dto;

import com.bank.models.TransactionType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.Instant;

@Data
public class TransactionDTO {
    private Long id;
    private Long userId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Instant operationTime;
    private Long sourceAccountId;
    private Long targetAccountId;
    private Double amountTransferred;
    private Integer mcc;
    private TransactionType transactionType;
}