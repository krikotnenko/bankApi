package com.bank.constants;

import java.util.List;


/**
 * The MCC code is a four-digit number that indicates what the company does, what goods
 * or services it sells: food, airline tickets, fuel, building materials, and others. It is assigned to the
 * seller when the store starts accepting bank cards for payment.
 * The bank uses the MCC code to credit bonuses or cashback, control
 * calculations and set limits on cards.
 */

public class MCCConstants {

    /**
     * Airplane tickets
     */
    public static List<Object> FLIGHTS = List.of(
            3000, 3001, 3002, 3003, 3004, 3005, 3006, 3007, 3008, 3009,
            3010, 3011, 3012, 3013, 3014, 3015, 3016, 3017, 3018, 3019,
            3020, 3021, 3022, 3023, 3024, 3025, 3026, 3027, 3028, 3029,
            3030, 3031, 3032, 3033, 3034, 3035, 3036, 3037, 3038, 3039,
            3040, 3041, 3042, 3043, 3044, 3045, 3046, 3047, 3048, 3049,
            3050, 3051, 3052, 3053, 3054, 3055, 3056, 3057, 3058, 3059,
            3060, 3061, 3062, 3063, 3064, 3065, 3066, 3067, 3068, 3069,
            3070, 3071, 3072, 3073, 3074, 3075, 3076, 3077, 3078, 3079,
            3080, 3081, 3082, 3083, 3084, 3085, 3086, 3087, 3088, 3089,
            3090, 3091, 3092, 3093, 3094, 3095, 3096, 3097, 3098, 3099,
            3100, 3101, 3102, 3103, 3104, 3105, 3106, 3107, 3108, 3109,
            3110, 3111, 3112, 3113, 3114, 3115, 3116, 3117, 3118, 3119,
            3120, 3121, 3122, 3123, 3124, 3125, 3126, 3127, 3128, 3129,
            3130, 3131, 3132, 3133, 3134, 3135, 3136, 3137, 3138, 3139,
            3140, 3141, 3142, 3143, 3144, 3145, 3146, 3147, 3148, 3149,
            3150, 3151, 3152, 3153, 3154, 3155, 3156, 3157, 3158, 3159,
            3160, 3161, 3162, 3163, 3164, 3165, 3166, 3167, 3168, 3169,
            3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179,
            3180, 3181, 3182, 3183, 3184, 3185, 3186, 3187, 3188, 3189,
            3190, 3191, 3192, 3193, 3194, 3195, 3196, 3197, 3198, 3199,
            3200, 3201, 3202, 3203, 3204, 3205, 3206, 3207, 3208, 3209,
            3210, 3211, 3212, 3213, 3214, 3215, 3216, 3217, 3218, 3219,
            3220, 3221, 3222, 3223, 3224, 3225, 3226, 3227, 3228, 3229,
            3230, 3231, 3232, 3233, 3234, 3235, 3236, 3237, 3238, 3239,
            3240, 3241, 3242, 3243, 3244, 3245, 3246, 3247, 3248, 3249,
            3250, 3251, 3252, 3253, 3254, 3255, 3256, 3257, 3258, 3259,
            3260, 3261, 3262, 3263, 3264, 3265, 3266, 3267, 3268, 3269,
            3270, 3271, 3272, 3273, 3274, 3275, 3276, 3277, 3278, 3279,
            3280, 3281, 3282, 3283, 3284, 3285, 3286, 3287, 3288, 3289,
            3290, 3291, 3292, 3293, 3294, 3295, 3296, 3297, 3298, 3299,
            3300, 3301, 3302, 3303, 3304, 3305, 3306, 3307, 3308, 3309,
            3310, 3311, 3312, 3313, 3314, 3315, 3316, 3317, 3318, 3319,
            3320, 3321, 3322, 3323, 3324, 3325, 3326, 3327, 3328, 3329,
            3330, 3331, 3332, 3333, 3334, 3335, 3336, 3337, 3338, 3339,
            3340, 3341, 3342, 3343, 3344, 3345, 3346, 3347, 3348, 3349,
            3350, 4304, 4415, 4418, 4511, 4582
    );


    /**
     * Sale, leasing, service and repair of cars, auto parts stores, motorcycles, car washes
     */
    public static final List<Integer> AUTO_SERVICES = List.of(
            5511, 5521, 5531, 5532, 5533, 5571, 7012, 7531, 7534, 7535,
            7538, 7542, 7549
    );

    public static final List<Integer> PHARMACIES = List.of(
            5122, 5292, 5295, 5912
    );

    /**
     * car rental agencies, motorhomes, trailers and trucks
     */
    public static final List<Integer> CAR_RENTAL = List.of(
            3351, 3352, 3353, 3354, 3355, 3356, 3357, 3358, 3359, 3360,
            3361, 3362, 3363, 3364, 3365, 3366, 3367, 3368, 3369, 3370,
            3371, 3372, 3373, 3374, 3375, 3376, 3377, 3378, 3379, 3380,
            3381, 3382, 3383, 3384, 3385, 3386, 3387, 3388, 3389, 3390,
            3391, 3392, 3393, 3394, 3395, 3396, 3397, 3398, 3400, 3401,
            3402, 3403, 3404, 3405, 3406, 3407, 3408, 3409, 3410, 3411,
            3412, 3413, 3414, 3415, 3416, 3417, 3418, 3419, 3420, 3421,
            3422, 3423, 3424, 3425, 3426, 3427, 3428, 3429, 3430, 3431,
            3432, 3433, 3434, 3435, 3436, 3437, 3438, 3439, 3441, 7512,
            7513, 7519
    );

    /**
     * Stores of building materials, furniture, household goods, such as IKEA and Leroy Merlin
     */
    public static final List<Integer> HOUSE_RENOVATION = List.of(
            1520, 1711, 1731, 1740, 1750, 1761, 1771, 1799, 2791, 2842,
            5021, 5039, 5046, 5051, 5065, 5072, 5074, 5085, 5198, 5200,
            5211, 5231, 5251, 5261, 5415, 5712, 5713, 5714, 5718, 5719,
            5722, 7622, 7623, 7629, 7641, 7641, 7692, 7699
    );

    /**
     * Long distance train tickets
     */
    public static final List<Integer> RAILWAY_TICKETS = List.of(
            4011, 4112
    );

    /**
     * Pet shops and veterinary services
     */
    public static final List<Integer> ANIMALS = List.of(
            0742, 5995
    );

    /**
     * Antique shops, arts and crafts shops
     */
    public static final List<Integer> ART = List.of(
            5932, 5937, 5970, 5971, 5972, 5973
    );

    /**
     * Cinemas and movie rentals
     */
    public static final List<Integer> MOVIE = List.of(
            7829, 7832, 7841
    );

    /**
     * Hairdressers and beauty salons, massage and cosmetics shops
     */
    public static final List<Integer> BEAUTY = List.of(
            5977, 7230, 7297, 7998
    );

    /**
     * Ambulance, hospitals, dental clinics, medical laboratories, orthopedic salons
     * and opticians
     */
    public static final List<Integer> MEDICAL_SERVICES = List.of(
            4119, 5047, 5296, 5975, 5976, 8011, 8021, 8031, 8041, 8042,
            8043, 8044, 8049, 8050, 8062, 8071, 8099, 8351, 8676
    );

    /**
     * Musical instrument stores, recording studios
     */
    public static final List<Integer> MUSIC = List.of(
            5733, 5735
    );

    /**
     * Cash withdrawal from ATMs
     */
    public static final List<Integer> CASH = List.of(
            6010, 6011
    );

    /**
     * Non-profit organizations - charitable foundations,
     * religious and political organizations
     */
    public static final List<Integer> NPO = List.of(
            8398, 8641, 8651, 8661, 8675, 8699, 9734, 8911, 8931, 8999
    );

    /**
     * Schools, colleges and universities, business courses and educational services
     */
    public static final List<Integer> EDUCATION = List.of(
            8211, 8220, 8241, 8244, 8249, 8299, 8493, 8494
    );

    /**
     * Clothing and footwear stores, jewelry stores, accessories, products
     * from leather and fur
     */
    public static final List<Integer> CLOTHES_SHOES = List.of(
            5094, 5137, 5139, 5611, 5621, 5631, 5641, 5651, 5661, 5681,
            5691, 5697, 5698, 5699, 5931, 5944, 5949, 5951, 7196, 7631
    );

    /**
     * Hotel rooms, hotels, motels, resorts, recreational
     * and sports camps, campsites
     */
    public static final List<Integer> HOTELS = List.of(
            3501, 3502, 3503, 3504, 3505, 3506, 3507, 3508, 3509, 3510,
            3511, 3512, 3513, 3514, 3515, 3516, 3517, 3518, 3519, 3520,
            3521, 3522, 3523, 3524, 3525, 3526, 3527, 3528, 3529, 3530,
            3531, 3532, 3533, 3534, 3535, 3536, 3537, 3538, 3539, 3540,
            3541, 3542, 3543, 3544, 3545, 3546, 3547, 3548, 3549, 3550,
            3551, 3552, 3553, 3554, 3555, 3556, 3557, 3558, 3559, 3560,
            3561, 3562, 3563, 3564, 3565, 3566, 3567, 3568, 3569, 3570,
            3571, 3572, 3573, 3574, 3575, 3576, 3577, 3578, 3579, 3580,
            3581, 3582, 3583, 3584, 3585, 3586, 3587, 3588, 3589, 3590,
            3591, 3592, 3593, 3594, 3595, 3596, 3597, 3598, 3599, 3600,
            3601, 3602, 3603, 3604, 3605, 3606, 3607, 3608, 3609, 3610,
            3611, 3612, 3613, 3614, 3615, 3616, 3617, 3618, 3619, 3620,
            3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629, 3630,
            3631, 3632, 3633, 3634, 3635, 3636, 3637, 3638, 3639, 3640,
            3641, 3642, 3643, 3644, 3645, 3646, 3647, 3648, 3649, 3650,
            3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660,
            3661, 3662, 3663, 3664, 3665, 3666, 3667, 3668, 3669, 3670,
            3671, 3672, 3673, 3674, 3675, 3676, 3677, 3678, 3679, 3680,
            3681, 3682, 3683, 3684, 3685, 3686, 3687, 3688, 3689, 3690,
            3691, 3692, 3693, 3694, 3695, 3696, 3697, 3698, 3699, 3700,
            3701, 3702, 3703, 3704, 3705, 3706, 3707, 3708, 3709, 3710,
            3711, 3712, 3713, 3714, 3715, 3716, 3717, 3718, 3719, 3720,
            3721, 3722, 3723, 3724, 3725, 3726, 3727, 3728, 3729, 3730,
            3731, 3732, 3733, 3734, 3735, 3736, 3737, 3738, 3739, 3740,
            3741, 3742, 3743, 3744, 3745, 3746, 3747, 3748, 3749, 3750,
            3751, 3752, 3753, 3754, 3755, 3756, 3757, 3758, 3759, 3760,
            3761, 3762, 3763, 3764, 3765, 3766, 3767, 3768, 3769, 3770,
            3771, 3772, 3773, 3774, 3775, 3776, 3777, 3778, 3779, 3780,
            3781, 3782, 3783, 3784, 3785, 3786, 3787, 3788, 3789, 3790,
            3791, 3792, 3793, 3794, 3795, 3796, 3797, 3798, 3799, 3800,
            3801, 3802, 3803, 3804, 3805, 3806, 3807, 3808, 3809, 3810,
            3811, 3812, 3813, 3814, 3815, 3816, 3817, 3818, 3819, 3820,
            3821, 3822, 3823, 3824, 3825, 3826, 3827, 7011, 7032, 7033
    );

    /**
     * Fines, court payments, alimony, taxes and fees
     */
    public static final List<Integer> PAYMENTS_TO_THE_BUDGET = List.of(
            9211, 9222, 9223, 9311, 9312, 9313, 9314, 9399, 9402, 9405,
            9751, 9752, 9950
    );

    /**
     * Tickets to the theater and concerts, bowling, billiards, sports clubs;
     * aquariums, dolphinariums and zoos, amusement parks, golf courses
     */
    public static final List<Integer> ENTERTAINMENT = List.of(
            7911, 7922, 7929, 7932, 7933, 7941, 7991, 7992, 7993, 7994,
            7996, 7997, 7998, 7999, 8664
    );

    /**
     * Department Stores, Non-Food, Software, Pawnshops, Stationery, Toy Stores
     */
    public static final List<Integer> MISCELLANEOUS_GOODS = List.of(
            5099, 5131, 5169, 5310, 5311, 5331, 5339, 5732, 5734, 5933,
            5935, 5943, 5945, 5948, 5978, 5993,
            5996, 5997, 5998, 5999, 7278, 7280
    );

    /**
     * Cafes, restaurants, canteens, bars and nightclubs
     */
    public static final List<Integer> RESTAURANTS = List.of(
            5811, 5812, 5813
    );

    /**
     * Telephony, money transfers, programming and data processing,
     * information retrieval, computer repair
     */
    public static final List<Integer> COMMUNICATION_TELECOM = List.of(
            4812, 4813, 4814, 4815, 4816, 4821, 4829, 4896, 4897, 4898,
            4899, 4901, 4902, 7372, 7375, 7479, 7894
    );

    /**
     * Couriers, transport companies, cleaning, laundries, dry cleaning, carpet
     * cleaning, shoe repair, photo services, advertising, consulting, lawyers
     * and legal services
     */
    public static final List<Integer> SERVICE_SERVICES = List.of(
            763, 780, 4214, 4215, 4225, 5199, 5299, 5301, 5543, 5544,
            5960, 5961, 5962, 5962, 5963, 5964, 5965, 5966, 5967, 5968,
            5969, 7013, 7210, 7211, 7216, 7217, 7221, 7251, 7311, 7321,
            7341, 7342, 7349, 7361, 7992, 7393, 7394, 7399, 7407, 8111
    );

    /**
     * Sportswear, bike shops and other sporting goods
     */
    public static final List<Integer> SPORTING_GOODS = List.of(
            5655, 5940, 5941
    );

    /**
     * Gift, souvenir and postcard shops
     */
    public static final List<Integer> SOUVENIRS = List.of(
            5947
    );

    /**
     * Super- and hypermarkets
     */
    public static final List<Integer> SUPERMARKETS = List.of(
            5297, 5298, 5300, 5411, 5412, 5422, 5441, 5451, 5462, 5499,
            5715, 5921
    );

    /**
     * Shopping at gas stations, even if you buy coffee there
     */
    public static final List<Integer> FUEL = List.of(
            5172, 5541, 5542, 5983
    );

    /**
     * City and intercity transport, taxi, car parking
     * sale of snowmobiles, motorhomes and trailers
     */
    public static final List<Integer> TRANSPORT = List.of(
            0011, 4111, 4121, 4131, 4457, 4468, 4784, 4789, 5013, 5271,
            5551, 5561, 5592, 5598, 5599, 7511, 7523
    );

    /**
     * Tour operators, cruises
     */
    public static final List<Integer> TRAVEL_AGENCIES = List.of(
            4411, 4416, 4417, 4419, 4722, 4723, 4761, 7015
    );

    /**
     * Fast food restaurants
     */
    public static final List<Integer> FAST_FOOD = List.of(
            5814
    );

    /**
     * Money transfers, insurance
     */
    public static final List<Integer> FINANCIAL_SERVICES = List.of(
            5416, 5417, 6012, 6050, 6051, 6211, 6300, 6381, 6399, 6513,
            6529, 6530, 6531, 6532, 6533, 6534, 6535, 6536, 6537, 6538,
            6540, 6611, 6760, 7322, 9411
    );

    /**
     * Computers, office equipment, photo studios and photo labs,
     * copy services
     */
    public static final List<Integer> PHOTO_VIDEO = List.of(
            5544, 5045, 5946, 7332, 7333, 7338, 7339, 7395
    );

    /**
     * Flower shops, floristry products
     */
    public static final List<Integer> FLOWERS = List.of(
            5193, 5992
    );

    /**
     * Various services, tax advice, debt advice,
     * funeral services
     */
    public static final List<Integer> PRIVATE_SERVICES = List.of(
            7014, 7261, 7273, 7276, 7277, 7295, 7299
    );

    /**
     * duty free shops
     */
    public static final List<Integer> DUTY_FREE = List.of(
            5309
    );

}
