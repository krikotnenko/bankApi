package com.bank.constants;

public class CashbackPercentValueConstants {

    public static final double increasedCashbackPercentValue = 0.03;
    public static final double defaultCashbackPercentValue = 0.01;

}
