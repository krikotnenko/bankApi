package com.bank.exceptions.handler;

import com.bank.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorObject> handleUserNotFoundException(UserNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "User is not found", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<ErrorObject> handleAccountNotFoundException(AccountNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "Account is not found", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TransactionNotFoundException.class)
    public ResponseEntity<ErrorObject> handleTransactionNotFoundException(TransactionNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "Transaction is not found", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AmountTransferredLessThanZeroException.class)
    public ResponseEntity<ErrorObject> handleServiceException(AmountTransferredLessThanZeroException e) {
        ErrorObject errorObject = new ErrorObject(500, "An error occurred while processing the request", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InsufficientAmountMoneyException.class)
    public ResponseEntity<ErrorObject> handleInSufficientAmountMoneyException(InsufficientAmountMoneyException e) {
        ErrorObject errorObject = new ErrorObject(105, "Not enough money to make a transfer on your account", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<ErrorObject> handleUserAlreadyExistsException(UserAlreadyExistsException e) {
        ErrorObject errorObject = new ErrorObject(502, "User already exists", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(AccountAlreadyExistsException.class)
    public ResponseEntity<ErrorObject> handleAccountAlreadyExistsException(AccountAlreadyExistsException e) {
        ErrorObject errorObject = new ErrorObject(502, "Account already exists", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(InitialBalanceLessThanZeroException.class)
    public ResponseEntity<ErrorObject> handleAccountInitialBalanceLessThanZeroException(InitialBalanceLessThanZeroException e) {
        ErrorObject errorObject = new ErrorObject(505, "Initial balance less than zero", Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(UserIsBlockedException.class)
    public ResponseEntity<ErrorObject> handleUserIsBlockedException(UserIsBlockedException e) {
        ErrorObject errorObject = new ErrorObject(412, "User ID is BLOCKED. Please contact the bank's technical support service to unblock your account", Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserIsBlockedAndInitialBalanceLessThanZeroException.class)
    public ResponseEntity<ErrorObject> handleUserIsBlockedAndInitialBalanceLessThanZeroException(UserIsBlockedAndInitialBalanceLessThanZeroException e) {
        ErrorObject errorObject = new ErrorObject(420, "User ID is BLOCKED and initial balance could not be less than zero. Please contact the bank's technical support service to unblock your account", Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TransactionAlreadyExistsException.class)
    public ResponseEntity<ErrorObject> handleTransactionAlreadyExistsException(TransactionAlreadyExistsException e) {
        ErrorObject errorObject = new ErrorObject(502, "Transaction already exists", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_GATEWAY.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(MCCCodeNotFoundException.class)
    public ResponseEntity<ErrorObject> handleMCCNotFoundException(MCCCodeNotFoundException e) {
        ErrorObject errorObject = new ErrorObject(404, "MCC code does not exist", Instant.now());
        errorObject.setStatusCode(HttpStatus.NOT_FOUND.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccountIDMatchException.class)
    public ResponseEntity<ErrorObject> handleAccountIDMatchException(AccountIDMatchException e) {
        ErrorObject errorObject = new ErrorObject(409, "The account IDs of the sender and recipient cannot be the same", Instant.now());
        errorObject.setStatusCode(HttpStatus.BAD_REQUEST.value());
        errorObject.setMessage(e.getMessage());
        errorObject.setTimestamp(Instant.now());
        return new ResponseEntity<>(errorObject, HttpStatus.BAD_REQUEST);
    }
}
