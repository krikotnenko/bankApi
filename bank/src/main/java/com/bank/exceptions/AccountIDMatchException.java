package com.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The account IDs of the sender and recipient cannot be the same")
public class AccountIDMatchException extends RuntimeException{
    public AccountIDMatchException(String message) {
        super(message);
    }
}
