package com.bank.exceptions;

public class UserIsBlockedAndInitialBalanceLessThanZeroException extends RuntimeException {
    public UserIsBlockedAndInitialBalanceLessThanZeroException(String msg) {
        super(msg);
    }
}
