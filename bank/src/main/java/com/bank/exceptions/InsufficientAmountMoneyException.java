package com.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Not enough money to make a transfer on your account")
public class InsufficientAmountMoneyException extends RuntimeException {
    public InsufficientAmountMoneyException(String message) {
        super(message);
    }
}