package com.bank.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "MCC code does not exist")
public class MCCCodeNotFoundException extends RuntimeException {
    public MCCCodeNotFoundException(Integer mcc) {
        super("MCC code " + mcc + " does not exist");
    }
}