package com.bank.exceptions;


public class UserIsBlockedException extends RuntimeException {
    public UserIsBlockedException(String msg) {
        super(msg);
    }
}