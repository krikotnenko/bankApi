package com.bank.exceptions;

public class InitialBalanceLessThanZeroException extends RuntimeException {
    public InitialBalanceLessThanZeroException(String msg) {
        super(msg);
    }
}