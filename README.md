## BankApi REST API

This project exposes some REST CRUD service for a BankApi App, including special methods like block/unblock user,
block/unblock/close account. It demonstrates the use of Spring boot, use integration and unit tests (Mockito,
Testcontainers), Docker, Flyway, Gradle.
An PostgreSQL database has been used to store the users, accounts and transactions data.

### Quick presentation

#### Create new user

    POST localhost:8080/api/users/

Body: JSON(application/json)

    {
        "email": "admin@admin.ru",
        "password": "admin",
        "firstName": "Admin",
        "lastName": "Adminov",
        "role": "ADMIN",
        "blocked": false
    }

Response: JSON

    {
        "id": 1,
        "email": "admin@admin.ru",
        "password": "admin",
        "firstName": "Admin",
        "lastName": "Adminov",
        "createdDate": "2022-06-16",
        "role": "ADMIN",
        "blocked": false
    }

#### Create new account

    POST localhost:8080/api/accounts/

Body: JSON(application/json)

    {
        "userId" : 1,
        "balance": 10000,
        "state": "ACTIVE"
    }

    {
        "userId" : 1,
        "balance": 7000,
        "state": "ACTIVE"
    }

Response: JSON

    {
        "id": 1,
        "userId": 1,
        "balance": 10000.0,
        "state": "ACTIVE",
        "dateCreated": "2022-06-16"
    }

    {
        "id": 2,
        "userId": 1,
        "balance": 7000.0,
        "state": "ACTIVE",
        "dateCreated": "2022-06-16"
    }

#### Make new transaction

    POST localhost:8080/api/transactions/

Body: JSON(application/json)

    {
        "userId": 1,
        "sourceAccountId": 2,
        "targetAccountId": 1,
        "amountTransferred": 5000
    }

Response: JSON

    There are no response due to void method makeTransaction() returns nothing. At the same time, transaction will change the balance on the accounts, what we can see from the table "transactions" in database and in console.

    | id | balance |             date           | state | userId |
    | ---|-------- | ---------------------------|-------|--------|
    | 1  |  15000  | 2022-06-16 00:55:07.026594 |ACTIVE |  1     |
    | 2  |  2000   | 2022-06-16 00:57:41.742539 |ACTIVE |  1     |

    Important : The project requires Gradle 7.x.x and a Java 8+ JDK.

__1. Clone the gitlab repository__

__2. Set up database configuration and Docker container for image: postgres:14.2 for integration tests.__

__3. Launch the REST server__